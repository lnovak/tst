﻿
namespace BelaBlok
{
    partial class Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.naziv = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bPocetak = new System.Windows.Forms.Button();
            this.bPovijest = new System.Windows.Forms.Button();
            this.bPostavke = new System.Windows.Forms.Button();
            this.bIzlaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // naziv
            // 
            this.naziv.AutoSize = true;
            this.naziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.naziv.Location = new System.Drawing.Point(247, 9);
            this.naziv.Name = "naziv";
            this.naziv.Size = new System.Drawing.Size(303, 73);
            this.naziv.TabIndex = 0;
            this.naziv.Text = "Bela Blok";
            this.naziv.UseWaitCursor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(343, 419);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Izradio : Leon Novak";
            // 
            // bPocetak
            // 
            this.bPocetak.Location = new System.Drawing.Point(309, 125);
            this.bPocetak.Name = "bPocetak";
            this.bPocetak.Size = new System.Drawing.Size(163, 23);
            this.bPocetak.TabIndex = 2;
            this.bPocetak.Text = "Početak";
            this.bPocetak.UseVisualStyleBackColor = true;
            this.bPocetak.Click += new System.EventHandler(this.bPocetak_Click);
            // 
            // bPovijest
            // 
            this.bPovijest.Location = new System.Drawing.Point(309, 176);
            this.bPovijest.Name = "bPovijest";
            this.bPovijest.Size = new System.Drawing.Size(163, 23);
            this.bPovijest.TabIndex = 3;
            this.bPovijest.Text = "Povijest";
            this.bPovijest.UseVisualStyleBackColor = true;
            this.bPovijest.Click += new System.EventHandler(this.bPovijest_Click);
            // 
            // bPostavke
            // 
            this.bPostavke.Location = new System.Drawing.Point(309, 232);
            this.bPostavke.Name = "bPostavke";
            this.bPostavke.Size = new System.Drawing.Size(163, 23);
            this.bPostavke.TabIndex = 4;
            this.bPostavke.Text = "Postavke";
            this.bPostavke.UseVisualStyleBackColor = true;
            this.bPostavke.Click += new System.EventHandler(this.bPostavke_Click);
            // 
            // bIzlaz
            // 
            this.bIzlaz.Location = new System.Drawing.Point(309, 283);
            this.bIzlaz.Name = "bIzlaz";
            this.bIzlaz.Size = new System.Drawing.Size(163, 23);
            this.bIzlaz.TabIndex = 5;
            this.bIzlaz.Text = "Izlaz";
            this.bIzlaz.UseVisualStyleBackColor = true;
            this.bIzlaz.Click += new System.EventHandler(this.bIzlaz_Click);
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bIzlaz);
            this.Controls.Add(this.bPostavke);
            this.Controls.Add(this.bPovijest);
            this.Controls.Add(this.bPocetak);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.naziv);
            this.Name = "Start";
            this.Text = "Bela Blok";
            this.Load += new System.EventHandler(this.Start_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label naziv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bPocetak;
        private System.Windows.Forms.Button bPovijest;
        private System.Windows.Forms.Button bPostavke;
        private System.Windows.Forms.Button bIzlaz;
    }
}

