﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BelaBlok
{
    public partial class Igra : Form
    {
        public Engine engine = new Engine();
        public Igra()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        public void Test()
        {
            engine.Tim1.Bodovi.Add(100);
            engine.Tim2.Bodovi.Add(100);
        }


        //U textboxe stavlja vrijednosti
        public void Osvjezi()
        {
            tTim1.Text = engine.STim1 + " : ";
            tTim2.Text = engine.STim2 + " : ";
            tMax.Text = engine.MaxBodovi.ToString();
            tBod1.Text = engine.Tim1.Pobjede.ToString();
            tBod2.Text = engine.Tim2.Pobjede.ToString();
            tVan1.Text = engine.Tim1.DoVan(engine.MaxBodovi).ToString();
            tVan2.Text = engine.Tim2.DoVan(engine.MaxBodovi).ToString();
            tBodovi1.Text = engine.Tim1.Suma().ToString();
            tBodovi2.Text = engine.Tim2.Suma().ToString();
            tRazlika.Text = engine.Razlika().ToString();
            lb1.Items.Clear();
            lb2.Items.Clear();
            foreach(int i in engine.Tim1.Bodovi)
            {
                lb1.Items.Add(i);
            }
            foreach (int i in engine.Tim2.Bodovi)
            {
                lb2.Items.Add(i);
            }
        }
        public bool Pobjeda()
        {
            if (engine.Tim1.Pobjeda(engine.MaxBodovi) && engine.Tim2.Pobjeda(engine.MaxBodovi))
            {
                if(engine.Tim1.Suma() > engine.Tim2.Suma())
                {
                    engine.Tim1.Pobjede++;
                    return true;
                }else if (engine.Tim1.Suma() < engine.Tim2.Suma())
                {
                    engine.Tim2.Pobjede++;
                    return true;
                }
                else
                {
                    return false;
                }
            }else if (engine.Tim1.Pobjeda(engine.MaxBodovi))
            {
                engine.Tim1.Pobjede++;
                return true;
            }else if (engine.Tim2.Pobjeda(engine.MaxBodovi))
            {
                engine.Tim2.Pobjede++;
                return true;
            }
            return false;
        }


        private void Igra_Load(object sender, EventArgs e)
        {
            engine.NovaPatrija();
            //Test();
            Osvjezi();
        }

        private void Igra_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void bUnos_Click(object sender, EventArgs e)
        {
            //Kreiranje nove unos forme

            Unos unos = new Unos();
            if (unos.ShowDialog(this) == DialogResult.Yes)
            {
                //Vađenje podataka iz dialoga
                engine.Tim1.Karte = unos.Karte1;
                engine.Tim1.Zvanja = unos.Zvanja1;
                engine.Tim1.ZapisiRundu();
                engine.Tim2.Karte = unos.Karte2;
                engine.Tim2.Zvanja = unos.Zvanja2;
                engine.Tim2.ZapisiRundu();
            }
            //Pražnjenje dialoga
            unos.Dispose();
            if (Pobjeda())
            {
                engine.ZapisiIgru();
                engine.Izbrisi();
            }
            Osvjezi();
        }

    }
}
