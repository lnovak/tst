﻿
namespace BelaBlok
{
    partial class Igra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bUnos = new System.Windows.Forms.Button();
            this.tBodovi1 = new System.Windows.Forms.TextBox();
            this.tBodovi2 = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lb1 = new System.Windows.Forms.ListBox();
            this.lb2 = new System.Windows.Forms.ListBox();
            this.scroll = new System.Windows.Forms.VScrollBar();
            this.tRazlika = new System.Windows.Forms.TextBox();
            this.tVan1 = new System.Windows.Forms.TextBox();
            this.tVan2 = new System.Windows.Forms.TextBox();
            this.tMax = new System.Windows.Forms.TextBox();
            this.tTim1 = new System.Windows.Forms.TextBox();
            this.tBod1 = new System.Windows.Forms.TextBox();
            this.tBod2 = new System.Windows.Forms.TextBox();
            this.tTim2 = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bUnos
            // 
            this.bUnos.Location = new System.Drawing.Point(12, 403);
            this.bUnos.Name = "bUnos";
            this.bUnos.Size = new System.Drawing.Size(345, 35);
            this.bUnos.TabIndex = 1;
            this.bUnos.Text = "Unos";
            this.bUnos.UseVisualStyleBackColor = true;
            this.bUnos.Click += new System.EventHandler(this.bUnos_Click);
            // 
            // tBodovi1
            // 
            this.tBodovi1.Location = new System.Drawing.Point(15, 377);
            this.tBodovi1.Name = "tBodovi1";
            this.tBodovi1.ReadOnly = true;
            this.tBodovi1.Size = new System.Drawing.Size(145, 20);
            this.tBodovi1.TabIndex = 4;
            this.tBodovi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tBodovi2
            // 
            this.tBodovi2.Location = new System.Drawing.Point(212, 377);
            this.tBodovi2.Name = "tBodovi2";
            this.tBodovi2.ReadOnly = true;
            this.tBodovi2.Size = new System.Drawing.Size(145, 20);
            this.tBodovi2.TabIndex = 5;
            this.tBodovi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.lb1);
            this.flowLayoutPanel1.Controls.Add(this.lb2);
            this.flowLayoutPanel1.Controls.Add(this.scroll);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 42);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(369, 304);
            this.flowLayoutPanel1.TabIndex = 12;
            // 
            // lb1
            // 
            this.lb1.FormattingEnabled = true;
            this.lb1.Location = new System.Drawing.Point(3, 3);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(168, 303);
            this.lb1.TabIndex = 0;
            // 
            // lb2
            // 
            this.lb2.FormattingEnabled = true;
            this.lb2.Location = new System.Drawing.Point(177, 3);
            this.lb2.Name = "lb2";
            this.lb2.Size = new System.Drawing.Size(168, 303);
            this.lb2.TabIndex = 1;
            // 
            // scroll
            // 
            this.scroll.Location = new System.Drawing.Point(348, 0);
            this.scroll.Name = "scroll";
            this.scroll.Size = new System.Drawing.Size(16, 304);
            this.scroll.TabIndex = 2;
            // 
            // tRazlika
            // 
            this.tRazlika.Location = new System.Drawing.Point(167, 352);
            this.tRazlika.Name = "tRazlika";
            this.tRazlika.ReadOnly = true;
            this.tRazlika.Size = new System.Drawing.Size(39, 20);
            this.tRazlika.TabIndex = 13;
            this.tRazlika.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tVan1
            // 
            this.tVan1.Location = new System.Drawing.Point(15, 352);
            this.tVan1.Name = "tVan1";
            this.tVan1.ReadOnly = true;
            this.tVan1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tVan1.Size = new System.Drawing.Size(39, 20);
            this.tVan1.TabIndex = 14;
            // 
            // tVan2
            // 
            this.tVan2.Location = new System.Drawing.Point(318, 354);
            this.tVan2.Name = "tVan2";
            this.tVan2.ReadOnly = true;
            this.tVan2.Size = new System.Drawing.Size(39, 20);
            this.tVan2.TabIndex = 15;
            this.tVan2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tMax
            // 
            this.tMax.Location = new System.Drawing.Point(167, 377);
            this.tMax.Name = "tMax";
            this.tMax.ReadOnly = true;
            this.tMax.Size = new System.Drawing.Size(39, 20);
            this.tMax.TabIndex = 16;
            this.tMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tTim1
            // 
            this.tTim1.Location = new System.Drawing.Point(15, 12);
            this.tTim1.Name = "tTim1";
            this.tTim1.ReadOnly = true;
            this.tTim1.Size = new System.Drawing.Size(141, 20);
            this.tTim1.TabIndex = 17;
            this.tTim1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tTim1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // tBod1
            // 
            this.tBod1.Location = new System.Drawing.Point(159, 12);
            this.tBod1.Name = "tBod1";
            this.tBod1.ReadOnly = true;
            this.tBod1.Size = new System.Drawing.Size(24, 20);
            this.tBod1.TabIndex = 18;
            this.tBod1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tBod2
            // 
            this.tBod2.Location = new System.Drawing.Point(333, 12);
            this.tBod2.Name = "tBod2";
            this.tBod2.ReadOnly = true;
            this.tBod2.Size = new System.Drawing.Size(24, 20);
            this.tBod2.TabIndex = 20;
            this.tBod2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tTim2
            // 
            this.tTim2.Location = new System.Drawing.Point(189, 12);
            this.tTim2.Name = "tTim2";
            this.tTim2.ReadOnly = true;
            this.tTim2.Size = new System.Drawing.Size(141, 20);
            this.tTim2.TabIndex = 19;
            this.tTim2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Igra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 450);
            this.Controls.Add(this.tBod2);
            this.Controls.Add(this.tTim2);
            this.Controls.Add(this.tBod1);
            this.Controls.Add(this.tTim1);
            this.Controls.Add(this.tMax);
            this.Controls.Add(this.tVan2);
            this.Controls.Add(this.tVan1);
            this.Controls.Add(this.tRazlika);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.tBodovi2);
            this.Controls.Add(this.tBodovi1);
            this.Controls.Add(this.bUnos);
            this.Name = "Igra";
            this.Text = "Igra";
            this.Load += new System.EventHandler(this.Igra_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bUnos;
        private System.Windows.Forms.TextBox tBodovi1;
        private System.Windows.Forms.TextBox tBodovi2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ListBox lb1;
        private System.Windows.Forms.ListBox lb2;
        private System.Windows.Forms.VScrollBar scroll;
        private System.Windows.Forms.TextBox tRazlika;
        private System.Windows.Forms.TextBox tVan1;
        private System.Windows.Forms.TextBox tVan2;
        private System.Windows.Forms.TextBox tMax;
        private System.Windows.Forms.TextBox tTim1;
        private System.Windows.Forms.TextBox tBod1;
        private System.Windows.Forms.TextBox tBod2;
        private System.Windows.Forms.TextBox tTim2;
    }
}