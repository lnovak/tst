﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BelaBlok
{
    public class Engine : PostavkePartije
    {
        public Tim Tim1 { get; set; }
        public Tim Tim2 { get; set; }

        public Engine()
        {
            CitajPostavke();
        }

        public void NovaPatrija()
        {
            Tim1 = new Tim(STim1);
            Tim2 = new Tim(STim2);
        }

        public string Razlika()
        {
            if (Tim1.Suma() > Tim2.Suma())
            {
                return (Tim1.Suma() - Tim2.Suma()).ToString();
            }
            return (Tim2.Suma() - Tim1.Suma()).ToString();
        }

        public void Reset()
        {
            Tim1.Karte = 0;
            Tim1.Zvanja = 0;
            Tim1.Zbroji();
            Tim2.Karte = 0;
            Tim2.Zvanja = 0;
            Tim2.Zbroji();
        }

        public void Izbrisi()
        {
            Tim1.Izbrisi();
            Tim2.Izbrisi();
        }

        public string Formatiraj(int bod)
        {
            if (bod < 10)
            {
                return "   " + bod.ToString();
            }else if (bod < 100)
            {
                return "  " + bod.ToString();
            }
            else if (bod < 1000)
            {
                return " " + bod.ToString();
            }
            else
            {
                return bod.ToString();
            }
        }
        public void ZapisiIgru()
        {
            try
            {
                using (StreamWriter wr = new StreamWriter("Povijest.txt", append: true))
                {
                    wr.WriteLine(STim1 +  "   |   "   + STim2);
                    wr.WriteLine("-----------");
                    for (int i = 0; i < Tim1.Bodovi.Count(); i++)
                    {
                        wr.WriteLine(Formatiraj(Tim1.Bodovi.ElementAt(i)) + " | " + Formatiraj(Tim2.Bodovi.ElementAt(i)));
                    }
                    wr.WriteLine("-----------");
                    wr.WriteLine(Formatiraj(Tim1.Suma()) + " | " + Formatiraj(Tim2.Suma()));
                    wr.WriteLine(Formatiraj(Tim1.Pobjede) + " : " + Formatiraj(Tim2.Pobjede));
                    wr.WriteLine("");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Greška kod pisanja igra: ");
                Console.WriteLine(e.Message);
            }
        }

        public void IzbrisiPovijest()
        {
            try
            {
                using (StreamWriter wr = new StreamWriter("Povijest.txt"))
                {
                    wr.Write("");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Greška kod pisanja igra: ");
                Console.WriteLine(e.Message);
            }
        }


        
        public string Povijest()
        {
            try
            {
                string povijest;
                using (StreamReader sr = new StreamReader("Povijest.txt"))
                {
                    povijest = sr.ReadToEnd();
                }
                return povijest;
            }
            catch (Exception e)
            {
                Console.WriteLine("Greška kod čitanje igre: ");
                Console.WriteLine(e.Message);
                return "Greška kod čitanje igre!";
            }
        }
        
    }
}
