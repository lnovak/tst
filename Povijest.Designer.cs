﻿
namespace BelaBlok
{
    partial class Povijest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tPovijest = new System.Windows.Forms.TextBox();
            this.bIzlaz = new System.Windows.Forms.Button();
            this.bIzbrisi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tPovijest
            // 
            this.tPovijest.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tPovijest.Location = new System.Drawing.Point(74, 12);
            this.tPovijest.Multiline = true;
            this.tPovijest.Name = "tPovijest";
            this.tPovijest.ReadOnly = true;
            this.tPovijest.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tPovijest.Size = new System.Drawing.Size(234, 381);
            this.tPovijest.TabIndex = 0;
            // 
            // bIzlaz
            // 
            this.bIzlaz.Location = new System.Drawing.Point(74, 399);
            this.bIzlaz.Name = "bIzlaz";
            this.bIzlaz.Size = new System.Drawing.Size(75, 23);
            this.bIzlaz.TabIndex = 1;
            this.bIzlaz.Text = "Izlaz";
            this.bIzlaz.UseVisualStyleBackColor = true;
            this.bIzlaz.Click += new System.EventHandler(this.bIzlaz_Click);
            // 
            // bIzbrisi
            // 
            this.bIzbrisi.Location = new System.Drawing.Point(233, 399);
            this.bIzbrisi.Name = "bIzbrisi";
            this.bIzbrisi.Size = new System.Drawing.Size(75, 23);
            this.bIzbrisi.TabIndex = 2;
            this.bIzbrisi.Text = "Izbriši";
            this.bIzbrisi.UseVisualStyleBackColor = true;
            this.bIzbrisi.Click += new System.EventHandler(this.bIzbrisi_Click);
            // 
            // Povijest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 450);
            this.Controls.Add(this.bIzbrisi);
            this.Controls.Add(this.bIzlaz);
            this.Controls.Add(this.tPovijest);
            this.Name = "Povijest";
            this.Text = "Povijest";
            this.Load += new System.EventHandler(this.Povijest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tPovijest;
        private System.Windows.Forms.Button bIzlaz;
        private System.Windows.Forms.Button bIzbrisi;
    }
}