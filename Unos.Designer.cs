﻿
namespace BelaBlok
{
    partial class Unos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tIgra1 = new System.Windows.Forms.TextBox();
            this.tIgra2 = new System.Windows.Forms.TextBox();
            this.tTim2 = new System.Windows.Forms.TextBox();
            this.tTim1 = new System.Windows.Forms.TextBox();
            this.tZvanja2 = new System.Windows.Forms.TextBox();
            this.tZvanja1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bOdustani = new System.Windows.Forms.Button();
            this.bZapisi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tIgra1
            // 
            this.tIgra1.Location = new System.Drawing.Point(12, 58);
            this.tIgra1.Name = "tIgra1";
            this.tIgra1.Size = new System.Drawing.Size(133, 20);
            this.tIgra1.TabIndex = 0;
            this.tIgra1.Click += new System.EventHandler(this.tIgra1_Click);
            this.tIgra1.TextChanged += new System.EventHandler(this.tIgra1_TextChanged);
            // 
            // tIgra2
            // 
            this.tIgra2.Location = new System.Drawing.Point(193, 58);
            this.tIgra2.Name = "tIgra2";
            this.tIgra2.Size = new System.Drawing.Size(134, 20);
            this.tIgra2.TabIndex = 2;
            this.tIgra2.Click += new System.EventHandler(this.tIgra2_Click);
            // 
            // tTim2
            // 
            this.tTim2.Location = new System.Drawing.Point(193, 12);
            this.tTim2.Name = "tTim2";
            this.tTim2.ReadOnly = true;
            this.tTim2.Size = new System.Drawing.Size(134, 20);
            this.tTim2.TabIndex = 4;
            this.tTim2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tTim1
            // 
            this.tTim1.Location = new System.Drawing.Point(12, 12);
            this.tTim1.Name = "tTim1";
            this.tTim1.ReadOnly = true;
            this.tTim1.Size = new System.Drawing.Size(133, 20);
            this.tTim1.TabIndex = 3;
            this.tTim1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tZvanja2
            // 
            this.tZvanja2.Location = new System.Drawing.Point(193, 94);
            this.tZvanja2.Name = "tZvanja2";
            this.tZvanja2.Size = new System.Drawing.Size(134, 20);
            this.tZvanja2.TabIndex = 6;
            // 
            // tZvanja1
            // 
            this.tZvanja1.Location = new System.Drawing.Point(12, 94);
            this.tZvanja1.Name = "tZvanja1";
            this.tZvanja1.Size = new System.Drawing.Size(133, 20);
            this.tZvanja1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Igra";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(151, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Zvanja";
            // 
            // bOdustani
            // 
            this.bOdustani.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bOdustani.Location = new System.Drawing.Point(12, 143);
            this.bOdustani.Name = "bOdustani";
            this.bOdustani.Size = new System.Drawing.Size(155, 46);
            this.bOdustani.TabIndex = 9;
            this.bOdustani.Text = "Odustani";
            this.bOdustani.UseVisualStyleBackColor = true;
            this.bOdustani.Click += new System.EventHandler(this.bOdustani_Click);
            // 
            // bZapisi
            // 
            this.bZapisi.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.bZapisi.Location = new System.Drawing.Point(173, 143);
            this.bZapisi.Name = "bZapisi";
            this.bZapisi.Size = new System.Drawing.Size(155, 46);
            this.bZapisi.TabIndex = 12;
            this.bZapisi.Text = "Zapiši";
            this.bZapisi.UseVisualStyleBackColor = true;
            this.bZapisi.Click += new System.EventHandler(this.bZapisi_Click);
            // 
            // Unos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 201);
            this.Controls.Add(this.bZapisi);
            this.Controls.Add(this.bOdustani);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tZvanja2);
            this.Controls.Add(this.tZvanja1);
            this.Controls.Add(this.tTim2);
            this.Controls.Add(this.tTim1);
            this.Controls.Add(this.tIgra2);
            this.Controls.Add(this.tIgra1);
            this.Name = "Unos";
            this.Text = "Unos";
            this.Load += new System.EventHandler(this.Unos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tIgra1;
        private System.Windows.Forms.TextBox tIgra2;
        private System.Windows.Forms.TextBox tTim2;
        private System.Windows.Forms.TextBox tTim1;
        private System.Windows.Forms.TextBox tZvanja2;
        private System.Windows.Forms.TextBox tZvanja1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bOdustani;
        private System.Windows.Forms.Button bZapisi;
    }
}